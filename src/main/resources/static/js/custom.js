var map = new Map();
var totalPrice = 0;

$(document).ready(function () {
	
	$('.pop').on('click', function() {
        $('.imagepreview').attr('src', $(this).find('img').attr('src'));
        $('#imagemodal').modal('show');   
    }); 

	var register = false;
	$('#username').on('keyup', function(){
		if($('#username').val().length != 0) {
			var x = $(this).val();
			  $.ajax({
		          type: "POST",
		          contentType: "application/json",
		          url: '/verify',
		          data: x,
		          dataType: "json",
		          cache: false,
		          success: function (data) {
		        	  
		        	  	if(data == true) {
		        	  		$('#exists').show();
		        	  		register = true;
		        	  	} else {
		        	  		$('#exists').hide();
		        	  		register = false;
		        	  	}
		        	  	if(register == false && $('#password').val().length != 0) {
		      			  $('#register').prop('disabled', false);
		      		  } else {
		      			  $('#register').prop('disabled', true);
		      		  }
		          },
		          error: function (e) {
		          }
		      });
		} else {
			$('#exists').hide();
		}
	})
	$('#password').on('keyup', function(){
		if(register == false && $('#password').val().length != 0) {
			  $('#register').prop('disabled', false);
		  } else {
			  $('#register').prop('disabled', true);
		  }
	})
		$('.saveProduct').on('click', function(){
			var productObj = {
					pid : $('#pid').val(),
					productName : $('#productName').val(),
					price : $('#price').val(),
					image : $('#imgName').text()
			}
		  $.ajax({
	          type: "POST",
	          contentType: "application/json",
	          url: '/admin/saveProduct',
	          data: JSON.stringify(productObj),
	          dataType: "json",
	          cache: false,
	          success: function (data) {
	        	  	window.location.reload();
	          },
	          error: function (e) {
	          }
	      });
	})
	$('.editProduct').on('click', function(){
		var x = $(this).val();
		  $.ajax({
	          type: "POST",
	          contentType: "application/json",
	          url: '/admin/editProduct',
	          data: x,
	          dataType: "json",
	          cache: false,
	          success: function (data) {
	        	  	$('#pid').val(data.pid);
	        	  	$('#productName').val(data.productName);
	        	  	$('#price').val(data.price);
	        	  	$('#imgName').text(data.image);
	          },
	          error: function () {
	          }
	      });
	})
	$('.deleteProduct').on('click', function(){
		var x = $(this).data("id");
		  $.ajax({
	          type: "POST",
	          contentType: "application/json",
	          url: '/admin/deleteProductNow',
	          data: JSON.stringify(x),
	          dataType: "json",
	          cache: false,
	          success: function (data) {
	        	  	if(!data) {
	        	  		$('#deleteModal').modal('show');
	        	  	} else {
	        	  		window.location.reload();
	        	  	}
	          },
	          error: function () {
	        	  	
	          }
	      });
	})
	$('.addProduct').on('click', function(){
		$('.submitProducts').prop('disabled', false);
		var prodName = $(this).data("prod");
		var price = $(this).data("price");
		if (!map.has(prodName)){
			map.set(prodName, [1, price]);
			totalPrice += price;
		} else {
			var i = map.get(prodName);
			map.set(prodName, [parseInt(i)+1, price]);
			totalPrice += price;
		}
		appendProd();
	})
});

function remove(html){
	var key = $(html).val();
	var price = ($(html).data("price"));
	$('#addedProducts').empty();
	if (map.has(key)){
		var item = map.get(key);
		if(item[0] >= 0){
			totalPrice -= price;
			map.set(key, [item[0]-1, price]);
		} 
	}
	appendProd();
	if ( $('#addedProducts').children().length == 0 ) {
		$('.submitProducts').prop('disabled', true);
	}
}

function appendProd() {
	$('#addedProducts').empty();
	map.forEach(function(item, key){
		if (item[0] > 0) {
			var k = "'" + key + "'";
			$('#addedProducts').append("<tr><td>"+key+"</td>"+"<td>"+item[0]+"</td>"+"<td><button class='btn btn-warning removeProducts' onclick='remove(this)' value='"+key+"' data-price='"+item[1]+"'>-</button></td>");
		}
	})
	$('#totalPrice').text(totalPrice);
}

$('.submitProducts').on('click', function(){
	var orderArr = [];
	map.forEach(function(value, key){
		if(value[0] > 0) {
			var quantity = value[0];
			var quantityTimesPrice = quantity*value[1];
			orderArr.push([key, [quantity, quantityTimesPrice]]);
		}
	});
	var order = {
			user : $('#user').val(),
			totalPrice : totalPrice,
			map : orderArr
	}
	$.ajax({
        type: "POST",
        contentType: "application/json",
        url: "users/checkout",
        data: JSON.stringify(order),
        dataType: "json",
        cache: false,
        error: function (e) {
        }
    });
})

$('.reloadPage').on('click', function(){
	window.location.reload();
})

function getFileData(){
	
	 $.ajax({
		    url: "admin/uploadFile",
		    type: "POST",
		    data: new FormData($("#upload-file-form")[0]),
		    enctype: 'multipart/form-data',
		    processData: false,
		    contentType: false,
		    cache: false,
		    success: function (data) {
		    		$('#imgName').text(data);
		    },
		    error: function () {
		      // Handle upload error
		      // ...
		    }
		  });
}



