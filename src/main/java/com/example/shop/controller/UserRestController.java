package com.example.shop.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.shop.model.Items;
import com.example.shop.model.Orders;
import com.example.shop.model.Products;
import com.example.shop.model.Users;
import com.example.shop.repository.OrderRepository;
import com.example.shop.repository.ProductRepository;
import com.example.shop.repository.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class UserRestController {
	
	@Autowired
	UserRepository ur;
	
	@Autowired
	ProductRepository pr;
	
	@Autowired
	OrderRepository or;
	
	@PostMapping("/verify")
	public boolean verifyUsername(@RequestBody String username) {
		boolean ok = false;
		if(username != "") {
			Users user = ur.findByUserName(username);
			if(user != null) {
				ok = true;
			} else {
				ok = false;
			}
		}
		return ok;
	}
	
	@GetMapping("/users")
	public List<Users> getAllUsers() {
		return  ur.findAll();
	}
	
	@PostMapping("/users/checkout")
	public void checkout(@RequestBody String orders) throws JsonProcessingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode actualObj = mapper.readTree(orders);
		String userName = actualObj.get("user").textValue();
		int totalPrice = actualObj.get("totalPrice").asInt();
		JsonNode items = actualObj.get("map");
		Users user = ur.findByUserName(userName);
		List<Items> products = new ArrayList<>();
		items.forEach(i -> {
			Items item = new Items(pr.findByProductName(i.get(0).asText()), i.get(1).get(0).asInt(), i.get(1).get(1).asInt());	
			products.add(item);
		});
		Orders order = new Orders(totalPrice, new Date(), user, products);
		or.save(order);
	}
	
	@PostMapping("/admin/editProduct")
	public Products getProduct(@RequestBody int pid) {
		return pr.findOne(pid);
	}

	@PostMapping(value = {"/admin/deleteProductNow"})
	public boolean deleteProduct(@RequestBody int pid) {
		try {
			pr.delete(pid);
			return true;	
		} catch (Exception e) {
			return false;
		}
	}
	
	@PostMapping(value = {"/admin/saveProduct"})
	public void saveProduct(@RequestBody Products product) {
		pr.save(product);
	}
	
	@PostMapping(value = {"admin/uploadFile"})
	@ResponseBody
	public String uploadFile(@RequestParam("uploadfile") MultipartFile uploadfile) {
	  
	String filename = "";
	  try {
	    filename = uploadfile.getOriginalFilename();
	    String directory = "/Users/UGIN/Downloads/shop/src/main/resources/static/images";
	    String filepath = Paths.get(directory, filename).toString();
	    
	    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
	    stream.write(uploadfile.getBytes());
	    stream.close();
	  }
	  catch (Exception e) {
	    System.out.println(e.getMessage());
	  }
	  
	  return filename;
	} 
	
}
