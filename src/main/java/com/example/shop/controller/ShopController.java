package com.example.shop.controller;

import java.security.Principal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.shop.model.Products;
import com.example.shop.model.Users;
import com.example.shop.repository.ProductRepository;
import com.example.shop.repository.UserRepository;


@Controller
public class ShopController {
	
	@Autowired
	UserRepository ur;
	
	@Autowired
	ProductRepository pr;
	
	@RequestMapping(value = {"/", "/login"})
	public String login() {
		return "/login";
	}
	
	@RequestMapping(value = {"/register"})
	public ModelAndView register(ModelAndView mav) {
		mav.addObject("registerUser", new Users());
		mav.setViewName("/register");
		return mav;
	}
	

	@PostMapping(value = {"/register"})
	public String saveUser(@ModelAttribute Users user) {
		user.setEnabled(true);
		user.setDateJoined(new Date());
		user.setRole("ROLE_USER");
		ur.save(user);
		return "redirect:/login";
	}
	
	@RequestMapping(value = {"/user"})
	public ModelAndView userIndex(ModelAndView mav, Principal principal) {
		mav.addObject("products", pr.findAll());
		mav.addObject("userName", principal.getName());
		mav.setViewName("/user");
		return mav;
	}
	
	@RequestMapping(value = {"/user/history"})
	public ModelAndView userHistory(ModelAndView mav, Principal principal) {
		Users user = ur.findByUserName(principal.getName());
		mav.addObject("orders", user.getOrders());
		mav.addObject("userName", principal.getName());
		mav.addObject("role", user.getRole());
		mav.setViewName("/user-history");
		return mav;
	}

	@RequestMapping(value = {"/admin"})
	public ModelAndView adminIndex(ModelAndView mav) {
		mav.addObject("products", pr.findAll());
		mav.addObject("product", new Products());
		mav.setViewName("/admin");
		return mav;
	}
	
	@RequestMapping(value = {"/admin/view-users"})
	public ModelAndView viewUsers(ModelAndView mav) {
		mav.addObject("users", ur.findAll());
		mav.setViewName("view-users");
		return mav;
	}
	
	@RequestMapping(value = {"/admin/view-transactions/{id}"})
	public ModelAndView viewUserTransactions(ModelAndView mav, @PathVariable int id, Principal principal) {
		Users user = ur.findByUserName(principal.getName());
		mav.addObject("orders", ur.findOne(id).getOrders());
		mav.addObject("role", user.getRole());
		mav.setViewName("user-history");
		return mav;
	}
	
	
	
	@RequestMapping("/logout")
	public String logoutPage () {
	    return "redirect:/login";
	}


}
