package com.example.shop.config;

import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

public class StaticResourceConfiguration extends WebMvcConfigurerAdapter{
    @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {

            String filePath = "/Users/UGIN/Downloads/shop/src/main/resources/static/images/";
            registry.addResourceHandler("/admin/img/**")
            .addResourceLocations("file:/"+filePath)
            .setCachePeriod(0);
        }
    }
