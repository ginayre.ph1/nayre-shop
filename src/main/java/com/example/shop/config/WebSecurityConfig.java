package com.example.shop.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;


@Configuration
@EnableAutoConfiguration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private AuthenticationHandler handler;
	
//	@Autowired
//	BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
		.usersByUsernameQuery("select user_name, password, enabled from users where user_name=?")
		.authoritiesByUsernameQuery("select user_name, role from users where user_name=?");
//		.passwordEncoder(bCryptPasswordEncoder);
    }
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.csrf().disable()
			.authorizeRequests()
			.antMatchers("/login/**").permitAll()
			.antMatchers("/register").permitAll()
			.antMatchers("/verify").permitAll()
			.antMatchers("/logout").permitAll()
			.antMatchers("/admin/**").hasRole("ADMIN")
			.antMatchers("/user/**").hasRole("USER")
			.anyRequest().authenticated()
			.and().formLogin()
			.successHandler(handler)
			.loginPage("/login").permitAll()
			.failureUrl("/login?error=true")
			.usernameParameter("userName")
			.passwordParameter("password")
			.and()
			.logout()
			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
            .logoutSuccessUrl("/login").permitAll() 
			.permitAll()
			.invalidateHttpSession(false) 
			.and().exceptionHandling()
			.accessDeniedPage("/");

		}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
	    web 
	       .ignoring()
	       .antMatchers("/static/**", "/templates/**");
	}

}


