package com.example.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.shop.model.Orders;

public interface OrderRepository extends JpaRepository<Orders, Integer> {

}
