package com.example.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.shop.model.Users;

public interface UserRepository extends JpaRepository<Users, Integer> {
	
	@Query(value = "SELECT * FROM users WHERE user_name = ?1", nativeQuery = true)
	Users findByUserName(String username);

}
