package com.example.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.shop.model.Products;

public interface ProductRepository extends JpaRepository<Products, Integer> {

	Products findByProductName(String asText);

}
