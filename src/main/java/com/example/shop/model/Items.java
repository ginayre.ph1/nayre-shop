package com.example.shop.model;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Embeddable
public class Items {
	
	@ManyToOne
	@JoinColumn(name="productId", referencedColumnName="pid")
	public Products product;
	private int quantity;
	private int totalPrice;
	
	public Products getProducts() {
		return product;
	}
	public void setProducts(Products products) {
		this.product = products;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	public Items(Products products, int quantity, int totalPrice) {
		super();
		this.product = products;
		this.quantity = quantity;
		this.totalPrice = totalPrice;
	}
	public Items() {
		super();
	}

}
