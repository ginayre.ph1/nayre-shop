package com.example.shop.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "oid", scope = Orders.class)
public class Orders {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int oid;
	private int totalPrice;
	private Date orderDate;
	
	@ManyToOne
	@JoinColumn(name="userId", referencedColumnName="uid")
	private Users user;
	
	@ElementCollection
	@CollectionTable(name = "items", joinColumns = @JoinColumn(name = "oid"))
	public List<Items> items = new ArrayList<Items>();

	public int getOid() {
		return oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public List<Items> getItems() {
		return items;
	}

	public void setItems(List<Items> items) {
		this.items = items;
	}

	public Orders(int oid, int totalPrice, Date orderDate, Users user, List<Items> items) {
		super();
		this.oid = oid;
		this.totalPrice = totalPrice;
		this.orderDate = orderDate;
		this.user = user;
		this.items = items;
	}

	public Orders(int totalPrice, Date orderDate, Users user, List<Items> items) {
		super();
		this.totalPrice = totalPrice;
		this.orderDate = orderDate;
		this.user = user;
		this.items = items;
	}

	public Orders() {
		super();
	}

	@Override
	public String toString() {
		return "Order [oid=" + oid + ", totalPrice=" + totalPrice + ", orderDate=" + orderDate + ", user=" + user
				+ ", items=" + items + "]";
	}

	
}
